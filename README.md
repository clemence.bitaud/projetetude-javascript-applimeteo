# Réalisation d'une page web donnant la météo d'une ville choisi

L'utilisateur rentre une ville dans la barre de recherche puis un appel à une API météo est réalisé avec Ajax.
Les informations météorologiques sur la ville sont ensuite affichées sur le site.
Les designs du site ont été réalisés avec Bootstrap.
